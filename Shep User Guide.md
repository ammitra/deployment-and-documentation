# Shep User's Guide

## Contents
[[_TOC_]]

## Accessing the UI
The web-based UI for Shep can be found at http://localhost/manager on the machine hosting the shep-ui container or its hostname/IP address if accessing from another machine. Port 80, the default for HTTP, is used, however this can be changed by modifying the port map for the container.

### WARNING
Currently it is possible for multiple users to access the same resource through the online software. This can lead to **LOSS OR CORRUPTION OF DATA** for other users on the online software as well as those who access the boards through other means. **ALWAYS** ensure you are the only user intending to access a given resource before starting your work. 

## Boards
In Shep, a `Board` is the representation of a single HERD instance, which generally relates to a single processing card.

### Registration
There currently exist two methods for registering a board: manual and automatic. Manual registration requires the user to provide all the necessary information for a board and input this through an API request or the UI. Automatic registration requires an extra container to run on the processing card prior to the `herd-control-app` and is available only on Kubernetes deployments.

On the homepage for the Shep board manager, there is a list of all currently registered boards. At the top right corner of the list there is a button to add a new board.

![List of boards.](static/img/board-list.png)

Upon clicking this button, a popup dialog box should show. Within this dialog box, enter the correct values for the IP address and board type before clicking "Confirm" to register the board.

![New board popup.](static/img/new-board-filled.png)

Note that the new board will initially show as being offline in the board list, this will be automatically be updated once the new board is successfully contacted by the heartbeat server.

![List of boards with new board.](static/img/board-list-new.png)

### Management

#### Delete a Board

To delete a board from the database, simply click the ![Icon to delete board](static/img/delete.png) icon next to the board entry.

![Single board row](static/img/board-row.png)

#### Update a Board
Currently the only way to update a board is through the API.

### Control
To access the control interface for a `Board`, click on the hyperlinked UUID for the `Board` you would like to access. On the board page, there are three tabs: Overview, FSMs and Commands. The Overview tab provides information for the devices connected to the HERD instance. FSMs allows for control of finite state machines within the instance and Commands allows you to send stateless commands.

#### Run a Command
To run a command from the UI, the configuration is done in 3 stages:
1. Select the device to run the command on

![](static/img/board-command-device.png)

2. Select the command to run

![](static/img/board-command-select.png)

3. Modify the configuration options for the command

![](static/img/board-command-config.png)

With the configuration complete, click "Send Command" to submit the command. The progress bar underneath the form should then start to update as the command runs. Once complete, the output of the command is shown in the "Output" section.

![](static/img/board-command-output.png)

#### FSMs
Finite State Machine control can be found under the `FSMs` tab within the board page. FSMs can be independently ocntrolled for each device on the board by selecting the appropriate device in the tabs beneath.

##### Enabling and Disabling an FSM
For each FSM registered to a device, there will be four buttons:

![](static/img/board-fsm-buttons.png)

Each button has a different function:
1. Engage/Disengage the FSM
2. Reset the FSM
3. Show the FSM details
4. Show the form to run a transition

NOTE: Before any transitions can be run, the FSM must first be engaged.

##### Running a Transition

The configuration for to run a transition is similar to that of running a command:

1. Select an available trasition from the dropdown menu

![](static/img/board-fsm-select.png)

2. Fill out out all of the necessary configuration options for the commands within the transition.

![](static/img/board-fsm-config.png)

3. Click "Run Transition" to send the request to the board.

The transition will then run and the full output of both the transition and its constituen t commands will be shown under the "Output" section.

## Applications
In Shep an `Application` is a collection of `Board` objects. Any commands or requests sent to an `Application` endpoint are forwarded onto every `Board` object within that `Application` and the results are then collated before being presented to the user.

### Management

#### New Application
To create a new `Application`, the process is similar to the manual registration of a `Board`.

On the homepage for the Shep board manager, there is a list of all applications. At the top right corner of the list there is a button to create a new application.

![](static/img/application-list.png)

Upon clicking this button, a popup dialog box should show. Within this dialog box, enter a name for application and select the board UUIDs to be included before clicking "Confirm" to register the application.

![](static/img/application-new.png)

The new application will then show up in the list of applications.

![](static/img/application-list-new.png)


### Control
The control interface for Applications has been designed to be functionally identical to that of a single board. As such, the procedure to control FSMs and run commands both looks and acts the same.

Currently, all boards must be in the same state for their FSMs in order for the FSM control to behave correctly.

If the application contains boards of different types, then the policy is currently that there will be not functionality to run commands on any of the boards. This is to ensure that any command sent to the boards is valid has the same configuration options across all the boards within the application. This behaviour is likely to change to be more accommodating in future releases.
