# Deployment and Documentation

User guides and example deployments for the Tracker Online Software stack.

## Contents
[[_TOC_]]

## Standalone Deployment

When started out with the online software stack, it is recommended to first use the standalone version. No special hardware is required, such as the Phase II processing cards, to get started. All aspects of the online software are run within docker containers on a single computer. This allows for you to gain an understanding of how all aspects of the software work together before deploying to real hardware.

The standalone stack contains three herd-dummy containers, which provide functional examples of all the endpoints on real HERD instances. At the Shep level, the stack has all the containers required to use Shep: the API server, a web-based UI, a database to store board information and a heartbeat server.

### Prerequisites
In order to get started with the standalone stack, the [docker](https://docs.docker.com/get-docker/) container runtime environment is required to be installed on a linux computer running the x86 architecture. 

Furthermore, [docker-compose](https://docs.docker.com/compose/install/) is also necessary for building the stack from the YAML file.

### Building the stack
The docker-compose YAML file for the standalone deployment can be found [here](deployments/standalone/docker-compose.yml). This file should either be downloaded or copied to a location on the local machine. The instructions for then starting the docker containers are as follows:
```shell
# First navigate to the directory containing the docker-compose.yml file
cd path-to-directory/

# Pull the images
docker-compose pull

# Start the containers
# (-d flag runs the containers in the background, omit this to see the logging output in the terminal)
docker-compose up -d
```
The shep web UI can then be found by opening a web browser and navigating to http://localhost/manager (or replace `localhost` with the computer's hostname/IP address if accessing from a different computer). To use a port other than the default for the HTTP interface, change the port mapping for the `nginx` container within the docker-compose YAML file and restart the containers by using the command `docker-compose up -d` from within the same directory as the YAML file.

Information on how to register boards and use the shep UI can be found [here](Shep User Guide.md).

The stack can be stopped by running the command `docker-compose down` from the directory containing the YAML file.

### Monitoring
A stack of [Loki](https://grafana.com/oss/loki/), [Promtail](https://grafana.com/docs/loki/latest/clients/promtail/) and [Grafana](https://grafana.com/oss/grafana/) is installed as part of the standalone example to serve as a monitoring solution. To be used, you need to navigate to http://localhost:3002 and login with the username `admin` and the password defined in `grafana_password.txt` (`P@ssw0rd` by default). From there you can navigate to Explore on the left to explore the logs of the system.

## HERD On Hardware + Shep Stack

The method for deploying the software a test stand with real processing cards is very similar to that of the standalone deployment described above. The difference being that no herd-dummy containers are run on the Shep computer and instead the HERD containers are run on the SoCs of the processing cards.

### Prerequisites
[Docker](https://docs.docker.com/get-docker/) must be installed on both the SoCs running the HERD instances as well as the computer runnig the Shep stack.

Furthermore, [docker-compose](https://docs.docker.com/compose/install/) is also necessary for building the Shep stack from the YAML file, but is not required for the SoCs.

### Running HERD on the ATCA boards

The HERD application (implemented in the `herd-control-app` repository) is an executable that loads SWATCH/HERD plugins, and provides a network interface that allows remote applications to run the commands and FSM transitions procedures declared in those plugins. There are separate plugin libraries for different flavours of ATCA board, which are packaged up in docker containers using a common GitLab CI pipeline; the following sections describe how to run these containers.

#### Serenity

The Serenity plugin builds on the board-agnostic EMP plugin, and hence registers commands and FSMs both for board-independent EMP configuration procedures and for Serenity-specific procedures. If the HERD app is run with [example-config.yml](https://gitlab.cern.ch/p2-xware/software/serenity-herd/-/blob/master/example-config.yml) as the configuration file, the control application will load the Serenity plugin and create two devices: one for the Artix (named `artix`), the other (named `x1`) for a daughter card at the X1 site running EMP-based firmware.

The control application should typically be run in a CI-built docker container from the Serenity plugin repository; this is the simplest method for using the plugin, as it doesn't require building the code, simply downloading a docker container. Critical info:

  * The image *must* always be run using the `/opt/cactus/bin/serenity/docker-run.sh` script (which wraps `docker run`, adding extra arguments to e.g. make device files accessible inside the container).
  * Image URL: `gitlab-registry.cern.ch/p2-xware/software/serenity-herd/herd-app:v0.1.0`
  * Configuration
    * SMASH config file: Mount as `/board.smash`
    * HERD app config file: Either mount as `/herd-config.yml` or override by specifying the container `CMD`

Example command, using a non-default config file with path `/path/to/herd-config.yml` on the host machine, mounted as `/herd-config.yml` in the container:
   ```shell
   /opt/cactus/bin/serenity/docker-run.sh -d -p 3000:3000 -v /path/to/myBoard.smash:/board.smash -v /path/to/herd-config.yml:/herd-config.yml gitlab-registry.cern.ch/p2-xware/software/serenity-herd/herd-app:v0.2.0 herd-config.yml
   ``` 

#### Apollo
`<Apollo instructions>`


### Running Shep
The docker-compose YAML file for the shep stack can be found [here](deployments/shep-docker/docker-compose.yml). This file should either be downloaded or copied to a location on the local machine. The instructions for then starting the docker containers are as follows:
```shell
# First navigate to the directory containing the docker-compose.yml file
cd path-to-directory/

# Pull the images
docker-compose pull

# Start the containers (-d flag runs the containers in the background, omit this to see the logging output in the terminal)
docker-compose up -d
```
The shep web UI can then be found by opening a web browser and navigating to http://localhost/manager (or replace `localhost` with the computer's hostname/IP address if accessing from a different computer).

Information on how to register boards and use the shep UI can be found [here](Shep User Guide.md)

## Project links

| Project | Status |
|---------|--------|
| [herd-control-app](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/) | [![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/-/commits/master) [![coverage report](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-control-app/-/commits/master) |
| [herd-dummy](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/) | [![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/-/commits/master) [![coverage report](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/herd-dummy/-/commits/master) |
| [shep-api-server](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/) | [![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/-/commits/master) [![coverage report](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-api-server/-/commits/master) |
| [shep-ui](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/) |  [![pipeline status](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/-/commits/master) [![coverage report](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/badges/master/coverage.svg)](https://gitlab.cern.ch/cms-tracker-phase2-onlinesw/shep-ui/-/commits/master) |
